﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using ActApp.Backend.WebSockets;    
using ActApp.Backend.Data;
using ActApp.Backend.Data.Tables;
using ActApp.Backend.Data.Queries;
using ActApp.Backend.Data.Repos;
using ActApp.Backend.Data.Views;
using ActApp.Backend.WebApi.Requests.Activities;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;

namespace ActApp.Backend.WebApi.Controllers
{   
    [Route("api/[controller]")]
    [ApiController]
    public class ActivitiesController : ControllerBase
    {
        private readonly IActivityRepository _activityRepository;
        private readonly IUserRepository _userRepository;
        private readonly IWebSocketNotifier _webSocketNotifier;
        private readonly IMapper _mapper;

        public ActivitiesController(IActivityRepository activityRepository, IUserRepository userRepository, IWebSocketNotifier webSocketNotifier, IMapper mapper)
        {
            _activityRepository = activityRepository;
            _userRepository = userRepository;
            _webSocketNotifier = webSocketNotifier;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates new Activity.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="request"></param>
        /// <response code="201">Success: Activity created</response>
        /// <response code="400">Error: Bad Request</response>
        /// <response code="401">Error: Unauthorized</response>
        /// <response code="422">Error: ActivityType does not exist</response>
        /// <response code="404">Error: Wrong LeaderId - user does not exist</response>
        /// <response code="422">Error: ActivityType does not exist</response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("")]
        [SwaggerResponse(201, type: typeof(CreateActivityResponse))]
        public async Task<IActionResult> CreateActivity([FromBody] CreateActivityRequest request)
        {
            var leaderId = new Guid(HttpContext.User.Identity.Name);
            
            var newActivity = new Activity()
            {
                Name = request.Name,
                Description = request.Description,
                Location =  new Location { Latitude = request.Location.Latitude, Longitude = request.Location.Longitude }.GetPoint(4326),
                StartDate = request.StartDate.GetValueOrDefault(),
                EndDate = request.EndDate.GetValueOrDefault(),
                MaxUserAmount = request.MaxUserAmount.GetValueOrDefault(),
                ActivityType = new ActivityType() { Name = request.ActivityTypeName }
            };
            
            ActivityUser newActivityLeader = new ActivityUser()
            {
                Id = Guid.NewGuid(),
                ActivityId = newActivity.Id,
                UserId = leaderId,
                RoleId = Consts.LeadActivityRoleId
            };
            
            newActivity.ActivityUsers.Add(newActivityLeader);

            var (createdActivityView, result) = await _activityRepository.CreateActivity(newActivity);

            switch (result)
            {
                case QueryResult.NotFound:
                    return StatusCode(404, $"User with Id '{leaderId}' does not exist'");
                case QueryResult.Forbidden:
                    return StatusCode(403, $"To create activities of type '{newActivity.ActivityType.Name}' you must be subscribed to it yourself");
                case QueryResult.WrongParams:
                    return StatusCode(422, $"ActivityType '{request.ActivityTypeName}' does not exist'"); 
                default:
                    await _webSocketNotifier.CreateActivityNotify(createdActivityView);
                    return StatusCode(201, new CreateActivityResponse()
                        {
                            ActivityId = createdActivityView.Id,
                            CreateDate = createdActivityView.CreateDate,
                            ResultMessage = $"Activity {createdActivityView.Name}' created successfully"
                        }
                    );
            }
        }
        
        /// <summary>
        /// Loads requested amount of closest Activities in the current ActivityCategory 
        /// - NO AUTH REQUIRED
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="trackPoint"></param>
        /// <param name="activityCategory">Example values: sports, other</param>
        /// <param name="activityTypes">Example values: tennis, soccer</param>
        /// <param name="startDates">Date format example: 2018-02-28</param>
        /// <param name="limit">How many activities to load</param>
        /// <response code="200">Success: Activities loaded successfully</response>
        /// <response code="402">Error: Category does not exist</response>
        /// <response code="404">Error: Activities not found</response>
        /// <response code="422">Error: Invalid query</response>
        [AllowAnonymous]
        [Authorize(AuthenticationSchemes = "Bearer")]  
        [HttpGet("")]
        [SwaggerResponse(200, Type = typeof(GetActivitiesResponse))]
        public async Task<IActionResult> GetActivities(
            [FromQuery] [Required] Location trackPoint,
            [FromQuery] [Required] string activityCategory,
            [FromQuery] string [] activityTypes,
            [FromQuery] DateTime [] startDates,
            [FromQuery] [Required] int limit
            )
        {
            if (activityTypes.Any(string.IsNullOrEmpty))
                return StatusCode(422,
                    "Error: Empty ActivityTypes in query. Specify values or pass null if you wish to omit this parameter");

            var getActivitiesQuery = new GetActivitiesQuery(trackPoint, activityCategory, activityTypes, startDates, limit);
            var (activities, result) = await _activityRepository.GetActivities(getActivitiesQuery);
            
            switch (result)
            {    
                case  QueryResult.WrongParams:
                    return StatusCode(402, $"Category '{activityCategory}' does not exist");
                case QueryResult.NotFound:
                    return StatusCode(404, "No activities found matching search criteria");
                default:
                    var response = new GetActivitiesResponse()
                    {
                        ResultMessage = "Activities loaded successfully",
                        ActivityAmount = activities.Count(),
                        Activities = activities
                    };
                    
                    return StatusCode(200, response);
            }
        }
        
        /// <summary>
        /// Deletes Existing Activity by Id.
        /// </summary> 
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <response code="200">Success: Activity deleted</response>
        /// <response code="401">Error: Unauthorized</response>
        /// <response code="404">Error: Activity not found</response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpDelete("{id}")]
        [SwaggerResponse(200, Type = typeof(DeleteActivityResponse))]
        public async Task<IActionResult> DeleteActivity([FromRoute] Guid id)
        {
            var userId = new Guid(HttpContext.User.Identity.Name);
            var user = await _userRepository.GetUserById(userId);
            if(user == null)
                return StatusCode(403, $"User with Id '{userId}' was not found");

            var activityForRemove = await _activityRepository.GetActivity(id);
            if (activityForRemove == null)
                return StatusCode(404, $"Activity with Id '{id}' was not found");

            // if user is not an admin we must check if he is an Activity leader
            if (user.Rank.Id != Consts.AdminRankId)
            { 
                var userInActivity = activityForRemove.ActivityUsers.FirstOrDefault(au => au.User.Id == user.Id);
                if (userInActivity == null || userInActivity.Role.Id != Consts.LeadActivityRoleId)
                    return StatusCode(406, $"You are not an admin or Activity leader, and cannot delete this Activity");
            }

            await _activityRepository.DeleteActivity(activityForRemove);

            var response = new DeleteActivityResponse()
            {
                ActivityId = activityForRemove.Id,
                ResultMessage = $"Activity {activityForRemove.Name} deleted successfully."
            };

            await _webSocketNotifier.DeleteActivityNotify(activityForRemove);
            
            return StatusCode(200, response);
        }
        
        /// <summary>
        /// Invite user to activity.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <response code="200">Success: User added to activity</response>
        /// <response code="401">Error: Unauthorized</response>
        /// <response code="404">Error: Activity not found</response>
        /// <response code="409">Error: User cannot be invited to this activity (conditions are not met)</response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPatch("{id}/invite")]
        public async Task<IActionResult> InviteUserToActivityRequest(
            [FromRoute] Guid id,
            [FromBody] InviteUserToActivityRequest request)
        {
            var senderId = new Guid(HttpContext.User.Identity.Name);
            
            if (senderId == request.ReceiverId)
                return StatusCode(409, $"You cannot invite yourself"); 
                
            var activity = await _activityRepository.GetActivity(id);
            if (activity == null) 
                return StatusCode(404, $"Activity with Id '{id}' was not found"); 
            
            if (activity.ActivityUsers.Count >= activity.MaxUserAmount)  
                return StatusCode(409, $"Activity is full");
            
            if (activity.ActivityUsers.Any(au => au.User.Id == request.ReceiverId))
                return StatusCode(409, $"User with Id '{request.ReceiverId}' is already in activity and cannot be invited"); 

            var sender = activity.ActivityUsers.FirstOrDefault(au => au.User.Id == senderId);
            if (sender == null)
                return StatusCode(409, $"You are not in activity and cannot invite other users to it"); 
            if (!sender.Role.CanInvite)
                return StatusCode(409, $"You don't have permission to invite users"); 
 
            var receiver = await _userRepository.GetUserById(request.ReceiverId);
            if (receiver == null)
                return StatusCode(404, $"User with Id '{request.ReceiverId}' was not found");
            
            activity.ActivityUsers.Add(
                    new ActivityUser()
                    {
                        Id = Guid.NewGuid(),
                        ActivityId = activity.Id,
                        UserId = receiver.Id,
                        RoleId = Consts.MateActivityRoleId
                    }
                );

            await _activityRepository.SaveActivity(activity);

            receiver.CurrentActivityCount += 1;
            await _userRepository.SaveUser(receiver);
            
            return StatusCode(200, "User added to activity");
        } 
        
        /// <summary>
        /// Join activity.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <response code="200">Success: User joined Activity</response>
        /// <response code="401">Error: Unauthorized</response>
        /// <response code="404">Error: Activity not found</response>
        /// <response code="405">Error: You are not subscribe to the ActivityType of this Activity</response>
        /// <response code="409">Error: User cannot join activity (conditions are not met)</response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPatch("{id}/join")]
        public async Task<IActionResult> JoinActivityRequest(
            [FromRoute] Guid id)
        {    
            var activity = await _activityRepository.GetActivity(id);
            if (activity == null) 
                return StatusCode(404, $"Activity with Id '{id}' was not found"); 
            
            if (activity.ActivityUsers.Count >= activity.MaxUserAmount)  
                return StatusCode(409, $"Activity is full");

            var userId = new Guid(HttpContext.User.Identity.Name);
    
            var you = await _userRepository.GetUserById(userId);
            if (you == null)
                return StatusCode(404, $"User with Id '{userId}' was not found");
                
            if (you.UserSubscriptions.All(us => us.ActivityType.Id != activity.ActivityTypeId))
                return StatusCode(405, $"You ({you.UserName}) must be subscribed to the ActivityType '{activity.ActivityType.Name}' to join Activity '{activity.Name}'");
            
            if (activity.ActivityUsers.Any(au => au.User.Id == userId))
                return StatusCode(409, $"You ({you.UserName}) are already in Activity '{activity.Name}'");
            
            activity.ActivityUsers.Add(
                new ActivityUser()
                {
                    Id = Guid.NewGuid(),
                    ActivityId = activity.Id,
                    UserId = userId,
                    RoleId = Consts.MateActivityRoleId
                }
            );
            
            await _activityRepository.SaveActivity(activity);

            you.CurrentActivityCount += 1;
            await _userRepository.SaveUser(you);
            
            return StatusCode(200, $"User {you.UserName} joined Activity {activity.Name}'");
        }
        
        /// <summary>
        /// Send message to Activity chat.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <response code="200">Success: Message sent</response>
        /// <response code="401">Error: Unauthorized</response>
        /// <response code="403">Error: You are not member of activity and cannot send messages to ActivityChat</response>
        /// <response code="404">Error: Activity not found</response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("{id}/message")]
        public async Task<IActionResult> SendChatMessageRequest(
            [FromRoute] Guid id,
            [FromBody] SendChatMessageRequest request)
        {
            Guid userId = new Guid(HttpContext.User.Identity.Name); 
            
            var activity = await _activityRepository.GetActivity(id);
            if (activity == null) 
                return StatusCode(404, $"Activity with Id '{id}' was not found");

            var sender = activity.ActivityUsers.FirstOrDefault(au => au.UserId == userId);    
            if(sender == null)
                return StatusCode(403, $"You are not a member of the activity '{activity.Name}' and cannot send messages to chat");

            var newChatMessage = new ChatMessage()
            {
                Id = Guid.NewGuid(),
                Text = request.Text,
                SendDate = DateTime.UtcNow,
                UserId = userId,
                User = sender.User
            };

            var msg = new ActivityChatMessage()
            {
                Id = Guid.NewGuid(),
                ActivityId = activity.Id,
                Activity = activity,
                ChatMessage = newChatMessage,
                ChatMessageId = newChatMessage.Id
            };
            
            activity.ActivityChatMessages.Add(
              msg
            );

            await _activityRepository.SaveActivity(activity);
            await _webSocketNotifier.ActivityChatMessageNotify(activity, newChatMessage);
        
            return StatusCode(200, $"Message sent");
        }
        
        /// <summary>
        /// Gets all Activity Chat user messages
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="id"></param>
        /// <response code="200">Success: Messages loaded</response>
        /// <response code="401">Error: Unauthorized</response>
        /// <response code="403">Error: You are not member of activity.</response>
        /// <response code="404">Error: Activity not found</response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("{id}/messages")]
        [SwaggerResponse(200, Type = typeof(GetChatMessagesResponse))]
        public async Task<IActionResult> GetChatMessagesRequest(
            [FromRoute] Guid id )
        {
            Guid userId = new Guid(HttpContext.User.Identity.Name); 
            
            var activity = await _activityRepository.GetActivity(id);
            if (activity == null) 
                return StatusCode(404, $"Activity with Id '{id}' was not found");
            
            var sender = activity.ActivityUsers.FirstOrDefault(au => au.UserId == userId);    
            if(sender == null)
                return StatusCode(403, $"You are not a member of the activity '{activity.Name}'");

            var resp = new GetChatMessagesResponse();

            foreach (var au in activity.ActivityUsers)
                resp.UserChats.Add(au.User.UserName, new List<ChatMessageView>());
            
            foreach (var userChat in resp.UserChats)
            {
                foreach (var cm in activity.ActivityChatMessages)
                {
                    string userName = cm.ChatMessage.User.UserName;
                    if (userName == userChat.Key)
                        resp.UserChats[userName].Add( _mapper.Map<ChatMessageView>(cm.ChatMessage));
                } 
            }
            
            return StatusCode(200, resp);
        }
    }
}