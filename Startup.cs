﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Reflection;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ActApp.Backend.WebApi.Auth;
using ActApp.Backend.Data;
using ActApp.Backend.Data.Repos;
using ActApp.Backend.WebApi.Swagger.Activities;
using ActApp.Backend.WebSockets;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.Swagger;
using WebSocketMiddleware = ActApp.Backend.WebSockets.WebSocketMiddleware;

namespace ActApp.Backend.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Data providers
            services.AddDbContext<ActAppDbContext>(options =>
            {
                options.UseNpgsql(Configuration["ConnectionStrings:Postgres"], o => o.UseNetTopologySuite());
            });
            services.AddHttpContextAccessor();
            services.AddScoped<IActivityRepository, ActivityRepository>();
            services.AddScoped<IActivityTypeRepository, ActivityTypeRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IConversationRepository, ConversationRepository>();
            
            // AutoMapper
            var assembly = Assembly.GetAssembly(typeof(MappingProfile));
            services.AddAutoMapper(assembly);
            
            // Ws functionality
            services.AddScoped<WebSocketMiddleware>();
            services.AddScoped<IWebSocketNotifier, WebSocketNotifier>();
            
            // Authorization
            services.Configure<JwtOptions>(options =>
            {
                options.TokenIssuer = Configuration["Auth:TokenIssuer"];
                options.TokenAudience = Configuration["Auth:TokenAudience"];
                options.TokenExpiresInDays = Configuration.GetValue<int>("Auth:TokenExpiresInDays");
                options.SecurityKey = Configuration["Auth:SecurityKey"];
            });
            services.AddScoped<ITokenHandler, JwtTokenHandler>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Auth:TokenIssuer"],
                        ValidAudience = Configuration["Auth:TokenAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(Configuration["Auth:SecurityKey"]))
                    };
                });
            
            // Swagger
            services.AddSwaggerGen(c =>
            {
                // Add description on main Swagger page 
                c.SwaggerDoc("v1", new Info
                {
                    Title = "ActApp.Backend.WebApi",
                    Version = "v1",
                    Description = "REST API for Android & IOS."
                });
                
                // Enable Auth functionality on Swagger page
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Please enter JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", Enumerable.Empty<string>() },
                });
                
                // Set the comments path for the Swagger JSON and UI.
                string xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                
                // enable Swagger request examples with auto annotation
                // https://github.com/mattfrear/Swashbuckle.AspNetCore.Filters#automatic-annotation
                c.ExampleFilters(); 
            });
            
            services.AddSwaggerExamplesFromAssemblyOf<CreateActivityRequestExample>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            
            // Websocket functionality
            app.UseWebSockets();
            app.UseMiddleware<WebSocketMiddleware>();
            
            // Swagger functionality
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ActApp.Backend.WebApi");
            });

            // disabled for Swagger
            // app.UseHttpsRedirection(); 
            app.UseMvc();
        }
    }
}