using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;

namespace ActApp.Backend.WebSockets
{
    public class WebSocketMiddleware : IMiddleware
    {
        public static ConcurrentDictionary<Guid, WebSocket> _sockets = new ConcurrentDictionary<Guid, WebSocket>();

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            //Check if it is a WS Upgrade request 
            if (!context.WebSockets.IsWebSocketRequest)
            {
                await next.Invoke(context);
                return;
            }
            
            // It IS  a WS Upgrade request. Begin token auth.
            var result = await context.AuthenticateAsync("Bearer");
            
            if (!result.Succeeded)
            {
                await context.ForbidAsync();
                return;
            }
            Console.WriteLine("Upgrade request authenticated");
            
            // Auth successfull. Now we can accept socket and begin message flow over WS protocol
            WebSocket currentSocket = await context.WebSockets.AcceptWebSocketAsync();
            
            Guid socketId = new Guid(context.Request.Headers["UserId"]);
            _sockets.TryAdd(socketId,  currentSocket);

            Console.WriteLine($"New Websocket connected with Id {socketId}");
            
            CancellationToken ct = context.RequestAborted;
            while (true) 
            {
                if (ct.IsCancellationRequested)
                    break;

                string message = await ReceiveAsync(currentSocket, ct);
                
                if(string.IsNullOrEmpty(message))
                {
                    if(currentSocket.State != WebSocketState.Open)
                        break;

                    continue;
                }

                await SendAllAsync(message, ct);
            }

            WebSocket dummy;
            _sockets.TryRemove(socketId, out dummy);

            await currentSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", ct);
            currentSocket.Dispose();
        }
        
        /// <summary>
        /// Sends message to open sockets (if socketIds is not null - we only send to these from socketIds)
        /// </summary>
        public async Task SendAllAsync(string data, CancellationToken ct = default(CancellationToken), Guid [] socketIds = null)
        {
            foreach (var socket in (socketIds == null ? _sockets : _sockets.Where(s => socketIds.Contains(s.Key))))
            {
                if(socket.Value.State != WebSocketState.Open)
                    continue;
                
                var buffer = Encoding.UTF8.GetBytes(data);
                var segment = new ArraySegment<byte>(buffer);

                await  socket.Value.SendAsync(segment, WebSocketMessageType.Text, true, ct);
            }
        }
        
        /// <summary>
        /// Sends message to a specific socket
        /// </summary>
        public async Task SendAsync(string data, Guid socketId, CancellationToken ct = default(CancellationToken))
        {
            var socket = _sockets.FirstOrDefault(s => s.Key == socketId);

            if (socket.Value.State != WebSocketState.Open) 
                return;
            
            var buffer = Encoding.UTF8.GetBytes(data);
            var segment = new ArraySegment<byte>(buffer);

            await  socket.Value.SendAsync(segment, WebSocketMessageType.Text, true, ct);
        }
   
        /// <summary>
        /// Receives message from socket 
        /// </summary>
        public static async Task<string> ReceiveAsync(WebSocket socket, CancellationToken ct = default(CancellationToken))
        {
            var buffer = new ArraySegment<byte>(new byte[8192]);
            
            using (var ms = new MemoryStream())
            {
                WebSocketReceiveResult result;
                do
                {
                    ct.ThrowIfCancellationRequested();

                    result = await socket.ReceiveAsync(buffer, ct);
                    ms.Write(buffer.Array, buffer.Offset, result.Count);
                }
                while (!result.EndOfMessage);

                ms.Seek(0, SeekOrigin.Begin);
                
                if (result.MessageType != WebSocketMessageType.Text)
                    return null;

                // Encoding UTF8: https://tools.ietf.org/html/rfc6455#section-5.6
                using (var reader = new StreamReader(ms, Encoding.UTF8))
                {
                    return await reader.ReadToEndAsync();
                }
            }
        }
    }
}